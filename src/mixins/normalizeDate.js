export default {
  methods: {
    normalizeDate (date, type) {
      const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
      const month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      let d = new Date(date)
      if (type === 'offer') {
        d = new Date(date.replace(' ', 'T'))
      }
      const n = d.getDay()
      const i = d.getDate()
      const m = d.getMonth()
      const y = d.getFullYear()
      const h = d.getHours()
      const min = d.getMinutes()

      let dateNew = days[n] + ' ' + i + ' ' + month[m] + ' ' + y + ' '

      if (type === 'offer') {
        dateNew = days[n] + ' ' + i + ' ' + month[m] + ' ' + y + ',' + ' '
      }
      const date1 = new Date().getTime()
      const date2 = d.getTime()
      const diff = date2 - date1
      const minutes = (diff / 1000) / 60
      const hours = minutes / 60
      if (Math.floor(hours) > 0 && Math.floor(hours) <= 48 && new Date().getDate() === i) {
        dateNew = 'Today, '
      }
      if (Math.floor(hours) > 0 && Math.floor(hours) <= 96 && new Date().getDate() + 1 === i) {
        dateNew = 'Tomorrow, '
      }
      if (Math.floor(hours) < 0 && Math.floor(hours) >= -48 && new Date().getDate() - 1 === i) {
        dateNew = 'Yesterday, '
      }
      let validMin
      if (min.toString().length < 2) {
        validMin = '0' + min.toString()
      } else {
        validMin = min.toString()
      }
      const time = h + ':' + validMin
      if (type === 'timeChat') {
        if (new Date().getDate() === i) {
          return 'Today'
        } else if (new Date().getDate() === i + 1) {
          return 'Yesterday'
        } else {
          return dateNew
        }
      }
      if (type === 'chat') {
        return {
          time,
          date: dateNew
        }
      }
      return dateNew + time
    }
  }
}

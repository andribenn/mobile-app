export default {
  data () {
    return {
      selects: {},
      timer: {
        driver: null,
        vehicle: null
      }
    }
  },
  methods: {
    toggleSelect (select, isItem) {
      this.timerCloseSelect(select, isItem)
      this.closeAllSelects()
      this.selects[select] = true
    },
    closeAllSelects () {
      const selectsData = Object.keys(this.selects)
      const data = this
      selectsData.forEach(function (item) {
        data.selects[item] = false
      })
    },
    closeAllSelectsEvent (e) {
      if (!e.target.classList.contains('select__touch')) {
        this.closeAllSelects()
      }
    },
    changeSelect (data, item, select, object) {
      this.selects[select] = false
      if (object !== undefined) {
        this[object][data] = item
        this.errors[object][data] = []
      } else {
        if (data === 'vehicle' || data === 'driver') {
          this[data] = item
          // navigator.notification.confirm(`Do you really want to change ${data}?`, (num) => {
          //   if (num === 2) {
          //     this[data] = item
          //   }
          // }, '', ['Cancel', 'OK'])
        } else {
          this[data] = item
        }
      }
    },
    timerCloseSelect (select, isItem) {
      clearTimeout(this.timer[select])
      if (isItem) {
        this.timer[select] = setTimeout(() => {
          this.selects[select] = false
        }, 2500)
      }
    }
  },
  created () {
    document.addEventListener('click', this.closeAllSelectsEvent)
  },
  beforeUnmount () {
    document.removeEventListener('click', this.closeAllSelectsEvent)
  }
}

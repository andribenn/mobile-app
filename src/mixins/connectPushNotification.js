import { api } from 'src/boot/axios'
import { PushNotifications } from '@capacitor/push-notifications'

export default {
  methods: {
    async connectPushNotification ({ device, id }) {
      try {
        const status = await PushNotifications.checkPermissions()
        if (status.receive === 'prompt') {
          await PushNotifications.requestPermissions()
        }
        if (status.receive !== 'granted') {
          return false
        }
        const after = async () => {
          await PushNotifications.addListener('registration', token => {
            if (device === null) {
              api.post('user/device/add', { token: token.value, user_id: id }).then(resp => {
                console.log(resp)
              })
            } else if (device.token !== token.value) {
              api.post('user/device/update', { token: token.value, id: device.id }).then(resp => {
                console.log(resp)
              })
            }
          })
          await PushNotifications.register()
        }
        if (status.receive === 'granted') {
          await after()
        }
      } catch (err) {
        alert(err)
      }
    }
  }
}

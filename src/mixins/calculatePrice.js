export default {
  methods: {
    calculatePrice (price) {
      return Math.round(Number(price) / 100 * 77)
    }
  }
}

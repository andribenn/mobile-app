import { Geolocation } from '@capacitor/geolocation'

export default {
  data () {
    return {
      positionID: null
    }
  },
  methods: {
    async checkPermissions () {
      return await Geolocation.checkPermissions().then((permissions) => {
        if (permissions.location !== 'granted') {
          Geolocation.requestPermissions({ permissions: ['location'] }).then(
            (response) => {
              console.log(response)
              return response.location === 'granted'
            }
          )
        } else {
          return true
        }
      })
    },
    async connectGPS () {
      await this.checkPermissions()
      Geolocation.getCurrentPosition().then((position) => {
        console.log(position)
      })
    },
    watchPosition () {
      this.positionID = Geolocation.watchPosition({}, (position, err) => {
        console.log(position)
        if (err) {
          console.log(err)
        }
      })
    },
    stopWatchPosition () {
      Geolocation.clearWatch({ id: this.positionID })
    }
  }
}

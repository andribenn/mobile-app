
const routes = [
  {
    path: '/',
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      { path: '', component: () => import('pages/login.vue') }
    ]
  },
  {
    path: '/geo',
    component: () => import('pages/GeolocationSlidePage')
  },
  {
    path: '/profile',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/profile/index.vue'),
        meta: {
          title: 'Profile',
          links: [{
            url: '/home',
            icon: require('../static/images/icons/arrow-back.svg'),
            position: 'left'
          }],
          hideNavMenu: true
        }
      },
      {
        path: 'edit',
        component: () => import('pages/profile/edit.vue'),
        meta: {
          title: 'Edit Profile',
          links: [{
            url: '/profile',
            icon: require('../static/images/icons/arrow-back.svg'),
            position: 'left'
          }],
          hideNavMenu: true
        }
      },
      {
        path: 'policy',
        component: () => import('pages/profile/policy.vue'),
        meta: {
          title: 'Privacy policy',
          links: [{
            url: '/profile',
            icon: require('../static/images/icons/arrow-back.svg'),
            position: 'left'
          }],
          hideNavMenu: true
        }
      },
      {
        path: 'gps',
        component: () => import('pages/profile/gps.vue'),
        meta: {
          title: 'GPS Tracking Policy',
          links: [{
            url: '/profile',
            icon: require('../static/images/icons/arrow-back.svg'),
            position: 'left'
          }],
          hideNavMenu: true
        }
      }
    ]
  },
  {
    path: '/chat',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/chat.vue'),
        meta: {
          title: 'mytripliner',
          links: [{
            url: '/home',
            icon: require('../static/images/icons/arrow-back.svg'),
            position: 'left'
          }],
          hideNavMenu: true
        }
      }
    ]
  },
  {
    path: '/home',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/home.vue') }
    ],
    meta: {
      title: 'mytripliner',
      links: [{
        url: '/profile',
        icon: require('../static/images/icons/avatar.svg'),
        position: 'right',
        subtitle: '4 trips'
      }]
    }
  },
  {
    path: '/offers',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/offers.vue') }
    ],
    meta: {
      title: 'Offers',
      subtitle: '4 trips'
    }
  },
  {
    path: '/offer/:id',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/offer.vue'), name: 'offer' }
    ],
    meta: {
      title: 'Offer',
      links: [{
        url: '/planned',
        icon: require('../static/images/icons/arrow-back.svg'),
        position: 'left'
      }],
      btns: [{
        type: 'btn',
        icon: require('../static/images/icons/extras.svg'),
        position: 'right'
      }],
      hideNavMenu: true
    }
  },
  {
    path: '/offer/remove/:id',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/remove-offer.vue'), name: 'offer-remove' }
    ],
    meta: {
      title: 'Emergency cancel',
      links: [{
        url: '/planned',
        icon: require('../static/images/icons/arrow-back.svg'),
        position: 'left'
      }],
      hideNavMenu: true
    }
  },
  {
    path: '/planned',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/planned.vue') }
    ],
    meta: {
      title: 'Planned',
      subtitle: '4 trips'
    }
  },
  {
    path: '/finished',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/finished.vue') }
    ],
    meta: {
      title: 'Finished',
      subtitle: '4 trips'
    }
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes

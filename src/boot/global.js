import { boot } from 'quasar/wrappers'
import connectGPS from 'src/mixins/connectGPS'

export default boot(({ app }) => {
  app.mixin(connectGPS)
  app.mixin({
    methods: {
      $mytriplineLink (path) {
        return (process.env.MYTRIPLINE_DOMAIN || 'https://mytripline.com') + '/' + path
      }
    }
  })
})

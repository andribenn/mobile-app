export default function () {
  return {
    isLoading: false,
    user: {},
    offers: [],
    planned: [],
    finished: [],
    adminStatus: 'offline',
    showAssignedOrders: false
  }
}

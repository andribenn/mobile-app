export function getLoadingStatus (state) {
  return state.isLoading
}

export function getUser (state) {
  return state.user
}

export function getOffers (state) {
  return state.offers.filter((item) => item.status === 'market')
}

export function getPlanned (state) {
  if (state.showAssignedOrders) {
    return state.planned.filter(item => item.driver?.user_id === state.user.id)
  } else {
    return state.planned
  }
}

export function getFinished (state) {
  const userId = state.user.id
  return state.finished.filter(item => item.user_id === userId || item.driver?.user_id === userId)
}

export function getAdminStatus (state) {
  return state.adminStatus
}

export function getShowAssignedOrders (state) {
  return state.showAssignedOrders
}

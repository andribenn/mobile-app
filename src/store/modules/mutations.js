export function setLoadingStatus (state, status) {
  state.isLoading = status
}

export function SET_USER (state, user) {
  state.user = user
}

export function SET_BOOKINGS (state, bookings) {
  const array = Object.values(bookings).flat()
  array.sort((a, b) => {
    return new Date(b.route_date) - new Date(a.route_date)
  })
  state.offers = array || []
  state.planned = bookings.planned || []
  state.finished = bookings.complete || []
}

export function SET_ADMIN_STATUS (state, status) {
  state.adminStatus = status
}

export function SET_SHOW_ASSIGNED_ORDERS (state, status) {
  state.showAssignedOrders = status
}

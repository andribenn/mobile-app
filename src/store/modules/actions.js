import { api } from 'src/boot/axios'

export async function getBookings ({ state, commit }) {
  commit('setLoadingStatus', true)
  await api.post('bookings/all', { user_id: state.user.id }).then(resp => {
    commit('SET_BOOKINGS', resp.data)
    commit('setLoadingStatus', false)
  })
}

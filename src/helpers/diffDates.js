export function diffHoursFromNow (d) {
  const diff = (new Date(d).getTime() - new Date().getTime()) / 3600000
  return Math.floor(diff)
}
